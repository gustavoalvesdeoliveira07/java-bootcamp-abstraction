package edu.dio.classes;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public class Mentoria extends Conteudo {

    private LocalDate data;


    // CONTRUCTOR
    public Mentoria(String titulo, String descricao, LocalDate data, String nivel) {
        super(titulo, descricao, nivel);
        this.data = data;
    }

    public Mentoria() {
        super();
    }

    // GETTERS
    public LocalDate getData() { return this.data; }

    // SETTERS
    public void setData() { this.data = data; }

    @Override
    public double calculaXP() {
        if (!NIVEL.contains(getNivel()))
            System.err.println("Nível informado incorreto: " + getNivel());

        return XP_PADRAO + 15.0d * (NIVEL.indexOf(getNivel()) + 1);
    }

    @Override
    public String toString() {
        return "Mentoria de " + getTitulo() + "\tData: " + getData() + "\nNível: " + getNivel() + "\n- " + getDescricao() +" \n\n";
    }
}
