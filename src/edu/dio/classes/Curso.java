package edu.dio.classes;

public class Curso extends Conteudo {

    private long cargaHoraria;

    // CONSTRUCTOR
    public Curso(String titulo, String descricao, long cargaHoraria, String nivel) {
        super(titulo, descricao, nivel);
        this.cargaHoraria = cargaHoraria;
    }

    // GETTER
    public long getCargaHoraria() { return this.cargaHoraria; }

    // SETTER
    public void setCargaHoraria(int cargaHoraria) { this.cargaHoraria = cargaHoraria; }

    @Override
    public double calculaXP() {
        if (!NIVEL.contains(getNivel()))
            System.err.println("Nível informado incorreto: " + getNivel());

        return XP_PADRAO * cargaHoraria + (NIVEL.indexOf(getNivel()) + 1);
    }

    @Override
    public String toString() {
        return "Curso de " + getTitulo() + "\tCH: " + getCargaHoraria() + "h" + "\n- " + getDescricao() + "\n\n";
    }
}
