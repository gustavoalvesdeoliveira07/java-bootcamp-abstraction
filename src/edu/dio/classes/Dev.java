package edu.dio.classes;

import java.util.*;

public class Dev {

    private String nome;
    private Set<Conteudo> conteudosInscritos = new LinkedHashSet<Conteudo>();
    private Set<Conteudo> conteudosFinalizados = new LinkedHashSet<Conteudo>();

    public void progredir() {
        Optional<Conteudo> conteudo = this.conteudosInscritos.stream().findFirst();

        if(conteudo.isPresent()) {
            this.conteudosInscritos.remove(conteudo.get());
            this.conteudosFinalizados.add(conteudo.get());
        } else {
            throw new Error("Você não está matriculado em nenhum conteúdo.");
        }
    }

    public double calculaTotalXP() {
        /*Iterator<Conteudo> conteudoIterator = this.conteudosFinalizados.iterator();
        double soma = 0d;
        while(conteudoIterator.hasNext()) {
            double conteudoXP = conteudoIterator.next().calculaXP();
            soma += conteudoXP;
        }
        return soma;*/
        return this.conteudosFinalizados.stream().mapToDouble(conteudo -> conteudo.calculaXP()).sum();

    }

    // CONSTRUCTOR
    public Dev(String nome, Set<Conteudo> conteudosInscritos, Set<Conteudo> conteudosFinalizados) {
        this.nome = nome;
        this.conteudosInscritos = conteudosInscritos;
        this.conteudosFinalizados = conteudosFinalizados;
    }

    public Dev() {}

    // GETTERS
    public String getNome() { return this.nome; }
    public Set<Conteudo> getConteudosInscritos() { return this.conteudosInscritos; }
    public Set<Conteudo> getConteudosFinalizados() { return this.conteudosFinalizados; }

    // SETTERS
    public void setNome(String nome) { this.nome = nome; }
    public void setConteudosInscritos(Set<Conteudo> conteudos) { this.conteudosInscritos = conteudos; }
    public void setConteudosFinalizados(Set<Conteudo> conteudos) { this.conteudosFinalizados = conteudos; }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null || getClass() != obj.getClass()) return false;

        Dev dev = (Dev) obj;
        return  Objects.equals(nome, dev.nome) &&
                Objects.equals(conteudosInscritos, dev.conteudosInscritos) &&
                Objects.equals(conteudosFinalizados, dev.conteudosFinalizados);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nome, conteudosInscritos, conteudosFinalizados);
    }

    @Override
    public String toString() {
        StringBuilder dev = new StringBuilder("Nome: " + this.nome + "\nConteúdos Inscritos: \n");

        for(Conteudo conteudo : this.conteudosInscritos)
            dev.append(conteudo);

        dev.append("Conteúdos Finalizados: \n");
        for(Conteudo conteudo : this.conteudosFinalizados)
            dev.append(conteudo);

        return dev.toString();
    }
}
