package edu.dio.classes;

import java.time.LocalDate;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

public class Bootcamp {

    private String nome;
    private String descricao;
    private long duracaoDias;
    private final LocalDate DATA_INICIO = LocalDate.now();
    private Set<Dev> devsInscritos = new LinkedHashSet<Dev>();
    private Set<Conteudo> conteudos = new LinkedHashSet<>();

    // CONSTRUCTOR
    public Bootcamp(String nome, String descricao, long duracao) {
        this.nome = nome;
        this.descricao = descricao;
        this.duracaoDias = duracao;
    }

    public Bootcamp(String nome, long duracao) {
        this.nome = nome;
        this.duracaoDias = duracao;
    }

    public Bootcamp() {
        this.duracaoDias = 30;
    }

    // GETTERS
    public String getNome() { return this.nome; }
    public String getDescricao() { return this.descricao; }
    public LocalDate getDataInicio() { return this.DATA_INICIO; }
    public long getDuracao() { return this.duracaoDias; }
    public Set<Dev> getDevsInscritos() { return this.devsInscritos; }
    public Set<Conteudo> getConteudos() { return this.conteudos; }
    //*
    public LocalDate getDataTermino() { return this.DATA_INICIO.plusDays(this.duracaoDias); }

    // SETTERS
    public void setNome(String nome) { this.nome = nome; }
    public void setDescricao(String descricao) { this.descricao = descricao; }
    public void setDevs(Set<Dev> devsInscritos) { this.devsInscritos = devsInscritos; }
    public void setConteudos(Set<Conteudo> conteudos) { this.conteudos = conteudos; }

    @Override
    public boolean equals(Object obj) {
        if(this == null) return true;
        if(obj == null || getClass() != obj.getClass()) return false;

        Bootcamp bootcamp = (Bootcamp) obj;
        return  Objects.equals(nome, bootcamp.nome) &&
                Objects.equals(DATA_INICIO, bootcamp.DATA_INICIO) &&
                Objects.equals(duracaoDias, bootcamp.duracaoDias) &&
                Objects.equals(devsInscritos, bootcamp.devsInscritos) &&
                Objects.equals(conteudos, bootcamp.conteudos);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nome, DATA_INICIO, duracaoDias, devsInscritos, conteudos);
    }

    @Override
    public String toString() {
        StringBuilder bootcamp = new StringBuilder("Bootcamp de " + this.nome + "\tDuração: " + getDataInicio() + " - " + getDataTermino() + "\n- Descrição: " + getDescricao() + "\nConteúdos: ");

        for(Conteudo conteudo : this.conteudos)
            bootcamp.append(conteudo);

        bootcamp.append("Inscritos: \n");
        for(Dev dev : this.devsInscritos)
            bootcamp.append(dev);

        return bootcamp.toString();
    }
}
