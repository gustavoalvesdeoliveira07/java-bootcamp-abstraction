package edu.dio.classes;

import java.util.Arrays;
import java.util.List;

public abstract class Conteudo {

    protected static final double XP_PADRAO = 10.0d;
    protected static final List<String> NIVEL = Arrays.asList("Básico", "Intermediário", "Avançado");

    private String titulo;
    private String descricao;
    private String nivel;

    public abstract double calculaXP();

    // CONSTRUCTOR
    public Conteudo(String titulo, String descricao, String nivel) {
        this.titulo = titulo;
        this.descricao = descricao;
        this.nivel = nivel;
    }

    public Conteudo() {}

    // GETTERS
    public String getTitulo() { return this.titulo; }
    public String getDescricao() { return this.descricao; }
    public String getNivel() { return this.nivel; }

    // SETTERS
    public void setTitulo(String titulo) { this.titulo = titulo; }
    public void setDescricao(String desc) { this.descricao = desc; }
    public void setNivel(String nivel) { this.nivel = nivel; }
}
