package edu.dio.main;

import edu.dio.classes.*;

import java.time.LocalDate;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        Set<Dev> devs = new LinkedHashSet<>(){{
            add(    new Dev("Gustavo",
                    new LinkedHashSet<Conteudo>(){{
                        add(new Curso("Laravel Avançado", "Aprendendo boas práticas com o framework Laravel - PHP", 14, "Avançado"));
                        add(new Mentoria("Design Patterns Laravel", "Principais padrões de projeto com exemplos aplicados no PHP/Laravel", LocalDate.now(), "Intermediário"));
                    }},
                    new LinkedHashSet<Conteudo>(){{
                        add(new Curso("BackEnd C#", "Aprendendo desenvolvimento BackEnd com a linguagem C# no ambiente .NET", 8, "Avançado"));
                    }}));

            add(    new Dev("Sara",
                    new LinkedHashSet<Conteudo>(){{
                        add(new Mentoria("Metodologias Ágeis: Estudo de Caso", "Aprendendo na prática como aplicar as principais Metodologias Ágeis", LocalDate.of(2023,8,1), "Avançado"));
                    }},
                    new LinkedHashSet<Conteudo>(){{
                        add(new Curso("Angular Avançado", "Desenvolvendo aplicativos FrontEnd com o framework Angular", 5, "Avançado"));
                    }}));
        }};

        Bootcamp bootcamp = new Bootcamp("Desenvolvimento FullStack Larangular ;)", "Aprendendo a desenvolver aplicações completas, desde o BackEnd (Laravel) até o FrontEnd(Angular)", 150);
        bootcamp.setConteudos(
                new LinkedHashSet<>(){{
                    add(new Mentoria("Design Patterns Laravel", "Principais padrões de projeto com exemplos aplicados no PHP/Laravel", LocalDate.now(), "Intermediário"));
                    add(new Mentoria("Metodologias Ágeis: Estudo de Caso", "Aprendendo na prática como aplicar as principais Metodologias Ágeis", LocalDate.of(2023,8,1), "Avançado"));
                    add(new Curso("Angular Avançado", "Desenvolvendo aplicativos FrontEnd com o framework Angular", 5, "Avançado"));
                }});
        bootcamp.setDevs(devs);

        System.out.println(bootcamp);
    }
}
